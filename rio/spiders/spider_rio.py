﻿# -*- coding: utf-8 -*-
import pymongo
import scrapy
from time import sleep
from seleniumrequests import PhantomJS
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from PIL import Image
from StringIO import StringIO
from rio.items import Funcionario, Remuneracao, Query
from retrying import retry
from py9kw.py9kw import py9kw
import logging


def retry_if_system_down(exception):
    logging.log(logging.ERROR,
                u'Ocorreu um problema, aguardando para tentar novamente')
    logging.log(logging.ERROR, exception)
    return (isinstance(exception, SystemDown) or
            isinstance(exception, TimeoutException) or
            isinstance(exception, NoSuchElementException))


def get_element_points(element):
    return (element.location['x'], element.location['y'],
            element.location['x'] + element.size['width'],
            element.location['y'] + element.size['height'])


class SystemDown(Exception):
    """O sistema indicou que não está respondendo agora"""
    pass


class RioSpider(scrapy.Spider):
    name = 'rio'
    allowed_domains = ['consultaremuneracao.rj.gov.br']
    start_urls = [
        'http://www.consultaremuneracao.rj.gov.br/pages/welcome.jsf'
    ]

    def __init__(self, *args, **kwargs):
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = (
            'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML,'
            'like Gecko) Chrome/48.0.2564.109 Safari/537.36')
        self.driver = PhantomJS(desired_capabilities=dcap)
        self.mongo = pymongo.MongoClient(kwargs['mongo_uri'])
        self.db = self.mongo[kwargs['mongo_db']]
        self.q = 'aa`'
        if kwargs.get('query_inicial'):
            self.q = kwargs['query_inicial']
        self.dups = 0
        self.increment_q()
        self.sem_dados = False
        self.second_query = False
        super(RioSpider, self).__init__(*args, **kwargs)

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items'),
            *args, **kwargs
        )

    def closed(self):
        self.log(u'TERMINOU, fechando browser', 20)
        self.driver.quit()

    def decrease_string(self, string):
        if string[-1] == 'a':
            return self.decrease_string(string[0:-1]) + 'z'
        else:
            return string[0:-1] + chr(ord(string[-1]) - 1)

    def increment_q(self):
        if self.q[2] < 'z':
            self.q = self.q[0:2] + chr(ord(self.q[2]) + 1)
        elif self.q[1] < 'z':
            self.q = self.q[0] + chr(ord(self.q[1]) + 1) + 'a'
        elif self.q[0] < 'z':
            self.q = chr(ord(self.q[0]) + 1) + 'aa'
        else:
            return None
        self.log(u'Query é: {}'.format(self.q), 20)
        if self.db.salarios_rio.find_one({'query': self.q}):
            self.log(u'Query {} já pesquisada, buscando próxima'.format(
                self.q), 20)
            self.increment_q()
        return True

    def prox_pesquisa(self):
        if self.increment_q() is None:
            raise StopIteration
        else:
            self.log(u'[prox_pesquisa] buscando usar o mesmo capctha.', 20)
            old_funcionarios_el = self.driver.find_elements_by_xpath(
                '//tbody/tr[@data-ri]')
            resp = self.driver.request(
                'post', 'http://www.consultaremuneracao.rj.gov.br/pages/' +
                'consulta.jsf', data=(
                    "consultaform=consultaform&javax.faces.ViewState={"
                    "view_state}&j_idt11=&j_idt13={q}&CaptchaID={"
                    "captcha_solved}&javax.faces.partial.ajax=true&javax.faces"
                    ".source=searchButton&javax.faces.partial.execute=@all"
                    "&javax.faces.partial.render=msgs&searchButton="
                    "searchButton").format(
                        view_state=self.state_pesquisa, q=self.q,
                        captcha_solved=self.captcha_solved), headers={
                            "X-Requested-With": "XMLHttpRequest",
                            "Faces-Request": "partial/ajax", "User-Agent":
                            ("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit"
                             "/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97"
                             "Safari/537.36"), "Content-Type":
                            "application/x-www-form-urlencoded", 'Referer':
                            ('http://www.consultaremuneracao.rj.gov.br/pages'
                                '/welcome.jsf')})
            if 'javax.faces.application.ViewExpiredException' in resp.text:
                self.log(u'Falhou uso do mesmo captcha. {}'.format(resp.text),
                         30)
                for item in self.parse(self.response):
                    yield item
            elif u'dados para a consulta solicitada' in resp.text:
                query = Query()
                self.dups = 0
                query['query'] = self.q
                query['n_results'] = 0
                self.log(u'Query {} terminada: {}'.format(self.q, query), 20)
                yield query
                for funcionario in self.prox_pesquisa():
                    yield funcionario
            else:
                self.log(u'Dados foram encontrados: {}'.format(resp.text), 20)
                self.driver.execute_script(
                    ('widget_j_idt9_resultado.'
                     'getPaginator().setRowsPerPage(100);'))
                WebDriverWait(self.driver, 15).until(EC.staleness_of(
                    old_funcionarios_el[0]))
                funcionarios_el = self.driver.find_elements_by_xpath(
                    '//tbody/tr[@data-ri]')
                for funcionario in self.funcionarios(funcionarios_el):
                    yield funcionario

    def remuneracoes(self, remuneracoes_el):
        n_remunera_lista = [el.get_attribute('data-ri') for el in
                            remuneracoes_el]
        for n_remunera in n_remunera_lista:
            WebDriverWait(self.driver, 15).until(
                EC.presence_of_element_located(
                    (By.XPATH, ("//tbody[@id='j_idt9:resultado_data']/tr["
                                "@data-ri={}]").format(n_remunera))))
            remuneracao_el = self.driver.find_element_by_xpath(
                "//tbody[@id='j_idt9:resultado_data']/tr[@data-ri={}]".format(
                    n_remunera))
            remuneracao = Remuneracao()
            remuneracao['vinculo'] = remuneracao_el.find_element_by_xpath(
                "./td[1]//tr[1]/td[2]").text
            remuneracao['servidor'] = remuneracao_el.find_element_by_xpath(
                "./td[1]//tr[2]/td[2]").text
            remuneracao['matricula'] = remuneracao_el.find_element_by_xpath(
                "./td[1]//tr[3]/td[2]").text
            remuneracao['descricao_do_vinculo'] = (
                remuneracao_el.find_element_by_xpath("./td[1]//tr[4]/td[2]")
                .text)
            remuneracao['cargo_efetivo'] = (
                remuneracao_el.find_element_by_xpath("./td[1]//tr[5]/td[2]")
                .text)
            remuneracao['cargo_comissionado'] = (
                remuneracao_el.find_element_by_xpath("./td[1]//tr[6]/td[2]")
                .text)
            remuneracao_el.find_element_by_xpath(
                ".//input[@value='Remuneração']").click()
            WebDriverWait(self.driver, 15).until(
                EC.staleness_of(remuneracao_el))
            remuneracao['topo'] = self.driver.find_element_by_xpath(
                "id('j_idt9:j_idt10_content')//h3").text
            remuneracao['mes'] = self.driver.find_element_by_xpath(
                "id('j_idt9:j_idt21_content')//h3").text
            remuneracao['mensal'] = self.driver.find_element_by_xpath(
                '//tr[@data-ri=0]/td[2]').text
            remuneracao['eventual'] = self.driver.find_element_by_xpath(
                '//tr[@data-ri=1]/td[2]').text
            remuneracao['descontos'] = self.driver.find_element_by_xpath(
                '//tr[@data-ri=2]/td[2]').text
            remuneracao['limite'] = self.driver.find_element_by_xpath(
                '//tr[@data-ri=3]/td[2]').text
            remuneracao['liquido'] = self.driver.find_element_by_xpath(
                '//tr[@data-ri=4]/td[2]').text
            voltar_remunera = self.driver.find_element_by_id(
                "j_idt9:searchButton")
            voltar_remunera.click()
            WebDriverWait(self.driver, 15).until(EC.staleness_of(
                voltar_remunera))
            self.log(u'Remuneração guardada', 20)
            yield remuneracao

    def funcionarios(self, funcionarios_el):
        n_func_lista = [el.get_attribute('data-ri') for el in funcionarios_el]
        query = Query()
        self.dups = 0
        query['query'] = self.q
        query['n_results'] = len(n_func_lista)
        self.log(u'Query trouxe {} resultados'.format(len(n_func_lista)), 20)
        for n_func in n_func_lista:
            funcionario_el = self.driver.find_element_by_xpath(
                '//tbody/tr[@data-ri={n_func}]/td/div/a'.format(n_func=n_func))
            if self.q.upper() not in funcionario_el.text:
                funcionario_el.click()
                voltar_servidor = self.driver.find_element_by_xpath(
                    "//input[@value='Voltar']").click()
                WebDriverWait(self.driver, 15).until(EC.staleness_of(
                    funcionario_el))
                funcionario_el = self.driver.find_element_by_xpath(
                    '//tbody/tr[@data-ri={n_func}]/td/div/a'.format(
                        n_func=n_func))
                if self.q.upper() not in funcionario_el.text:
                    yield self.parse(self.response)
            funcionario = Funcionario()
            funcionario['nome'] = funcionario_el.text
            funcionario['cpf'] = self.driver.find_element_by_xpath(
                "//tbody/tr[@data-ri={n_func}]/td[1]/div".format(
                    n_func=n_func)).text
            if self.db.salarios_rio.find_one(dict(funcionario)) is None:
                try:
                    funcionario_el.click()
                    WebDriverWait(self.driver, 2).until(EC.staleness_of(
                        funcionario_el))
                except TimeoutException:
                    funcionario_el.click()
                    WebDriverWait(self.driver, 15).until(EC.staleness_of(
                        funcionario_el))
                if ('nculos para esse servidor' in
                        self.driver.find_element_by_id('j_idt9:msgs').text):
                    self.log((u'Funcionário sem vínculo.'
                              u'Pulando este funcionário'), 20)
                    continue
                funcionario['remuneracoes'] = []
                remuneracoes_el = self.driver.find_elements_by_xpath(
                    "//tbody[@id='j_idt9:resultado_data']/tr")
                self.log((u'Encontradas {} remunerações para este '
                          u'funcionário').format(len(remuneracoes_el)), 20)
                funcionario['remuneracoes'].extend(self.remuneracoes(
                    remuneracoes_el))
                funcionario['queries'] = [self.q]
                WebDriverWait(self.driver, 10).until(
                    EC.element_to_be_clickable(
                        (By.XPATH, "//input[@value='Voltar']")))
                voltar_servidor = self.driver.find_element_by_xpath(
                    "//input[@value='Voltar']")
                voltar_servidor.click()
                WebDriverWait(self.driver, 35).until(EC.staleness_of(
                    voltar_servidor))
                WebDriverWait(self.driver, 8).until(EC.element_to_be_clickable(
                    (By.XPATH, ('//tbody/tr[@data-ri={n_func}]/td/div/a')
                        .format(n_func=0))))
                old_funcionario_el = self.driver.find_element_by_xpath(
                    '//tbody/tr[@data-ri={n_func}]/td/div/a'.format(n_func=0))
                self.driver.execute_script(
                    ('widget_j_idt9_resultado.getPaginator()'
                     '.setRowsPerPage(100);'))
                WebDriverWait(self.driver, 40).until(EC.staleness_of(
                    old_funcionario_el))
                WebDriverWait(self.driver, 8).until(EC.element_to_be_clickable(
                    (By.XPATH, ('//tbody/tr[@data-ri={n_func}]/td/div/a')
                        .format(n_func=n_func))))
                self.log(u'Funcionário {} guardado: {}.'.format(
                    n_func, funcionario['nome']), 20)
            yield funcionario
        query['duplicates'] = self.dups
        self.log(u'Query {} terminada: {}'.format(self.q, query), 20)
#        last_q = self.db.salarios_rio.find_one({
#            'query': self.decrease_string(self.q)})
#        self.log(u'Verificando query anterior: {}'.format(
#            self.decrease_string(self.q)), 20)
#        if last_q:
#            if (last_q['n_results'] == query['n_results'] and
#                    last_q['duplicates'] == query['duplicates'] and not
#                    self.second_query):
#                self.log((u'Query anterior tem os mesmos resultados que atual'
#                          u'reiniciando busca por risco de erro do servidor'),
#                         20)
#                self.second_query = True
#                self.parse(self.response)
#        else:
#            self.log(u'Query anterior não encontrada na base', 20)
        yield query

        for funcionario in self.prox_pesquisa():
            yield funcionario

    def look_for_captcha_error(self):
        msgs = self.driver.find_elements_by_id('msgs')
        if msgs:
            if 'digo de seguran' in msgs[0].get_attribute('textContent'):
                self.kw.captcha_correct(False)
                self.log(u'Captcha {} errado reiniciando.'.format(
                    self.captcha_solved, 30))
                return self.parse(self.response)
        elif not self.captcha_manual:
            self.kw.captcha_correct(True)

    def error_or_passou(self, driver):
        if EC.staleness_of(self.captcha)(driver):
            return True
        try:
            msgs_text = self.driver.find_element_by_id('msgs').get_attribute(
                'textContent')
            if ('dados para a consulta solicitada' in msgs_text or
                'digo de seguran' in msgs_text or 'ViewExpiredException' in
                    msgs_text or 'Falha no acesso aos dados' in msgs_text):
                return True
            else:
                return False
        except StaleElementReferenceException:
            return False

    def sem_dadosf(self):
        query = Query()
        query['query'] = self.q
        query['n_results'] = 0
        self.increment_q()
        yield query
        self.sem_dados = True
        for item in self.parse(self.response):
            yield item

    @retry(wait_exponential_multiplier=140000, wait_exponential_max=15600000,
           retry_on_exception=retry_if_system_down)
    def parse(self, response):
        if self.sem_dados is False:
            self.response = response
            self.driver.get(response.url)
            (self.driver.find_element_by_xpath('//input[@type="submit"]')
                .click())
            WebDriverWait(self.driver, 15).until(
                EC.presence_of_element_located((By.TAG_NAME, 'img')))
            self.captcha = self.driver.find_element_by_xpath('//img')
            self.state_pesquisa = self.driver.find_element_by_id(
                'javax.faces.ViewState').get_attribute('value')
            WebDriverWait(self.driver, 15).until(
                EC.visibility_of(self.captcha))
            screen = Image.open(StringIO(self.driver.get_screenshot_as_png()))
            self.captcha_img = screen.crop(get_element_points(self.captcha))
            self.kw = py9kw('2HVITTUOV55Z7EYRQ1', verbose=True)
            img_bytes = StringIO()
            self.captcha_img.save(img_bytes, format='PNG')
            self.kw.uploadcaptcha(img_bytes.getvalue(), maxtimeout=450, prio=5)
            [self.kw.sleep((i+1)*30) for i in range(5) if self.kw.string is
                None or self.kw.string == "NO DATA"]
            self.captcha_solved = self.kw.string
            self.captcha_manual = False
            if self.captcha_solved == 'NO DATA':
                self.log(u'Captcha não resolvido, reiniciando.', 30)
                return self.parse(self.response)
            captcha_input = self.driver.find_element_by_name('CaptchaID')
            captcha_input.send_keys(self.captcha_solved)
        nome = self.driver.find_element_by_xpath('//tr[2]/td[2]/input')
        nome.clear()
        nome.send_keys(self.q)
        self.driver.execute_script((
            '$(document).ajaxComplete('
            '    function(e, xhr){'
            "        $('#msgs').append($(xhr.responseXML.children[0])"
            "           .children());"
            '    }'
            ');'))
        sleep(2)
        buscar = self.driver.find_element_by_id('searchButton')
        buscar.click()
        self.log(u'Fazendo busca', 20)
        WebDriverWait(self.driver, 125).until(self.error_or_passou)
        self.look_for_captcha_error()
        error = self.driver.find_elements_by_id('msgs')
        if error:
            msg = error[0].get_attribute('textContent')
            if u'dados para a consulta solicitada' in msg:
                self.log(u'Não há dados para a query, iniciando nova query.',
                         20)
                self.driver.execute_script("$('#msgs').empty();")
                return self.sem_dadosf()
            if 'ViewExpiredException' in msg:
                self.log((u'Não foi possível recuperar o uso da view, '
                          u'reiniciando.'), 20)
                return self.parse(self.response)
            else:  # erro de sistema
                self.log(u'O sistema aprsentou um erro: {}'.format(msg), 20)
                raise SystemDown
        WebDriverWait(self.driver, 15).until(EC.presence_of_element_located(
            (By.XPATH, '//tbody/tr[@data-ri={n_func}]/td/div/a'.format(
                n_func=0))))
        old_funcionario_el = self.driver.find_element_by_xpath(
            '//tbody/tr[@data-ri={n_func}]/td/div/a'.format(n_func=0))
        sleep(2)
        self.driver.execute_script(
            'widget_j_idt9_resultado.getPaginator().setRowsPerPage(100);')
        WebDriverWait(self.driver, 15).until(EC.staleness_of(
            old_funcionario_el))
        funcionarios_el = self.driver.find_elements_by_xpath(
            '//tbody/tr[@data-ri]')
        self.sem_dados = False
        try:
            return self.funcionarios(funcionarios_el)
        except:
            return self.parse(self.response)
