# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Funcionario(scrapy.Item):
    nome = scrapy.Field()
    cpf = scrapy.Field()
    remuneracoes = scrapy.Field()
    queries = scrapy.Field()
    
    
class Remuneracao(scrapy.Item):
    vinculo = scrapy.Field()
    servidor = scrapy.Field()
    cargo_efetivo = scrapy.Field()
    matricula = scrapy.Field()
    descricao_do_vinculo = scrapy.Field()
    cargo_comissionado = scrapy.Field()
    topo = scrapy.Field()
    mes = scrapy.Field()
    mensal = scrapy.Field()
    eventual = scrapy.Field()
    descontos = scrapy.Field()
    limite = scrapy.Field()
    liquido = scrapy.Field()
    
    
class Query(scrapy.Item):
    query = scrapy.Field()
    duplicates = scrapy.Field()
    n_results = scrapy.Field()
